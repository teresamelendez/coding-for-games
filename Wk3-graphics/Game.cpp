//game.cpp

#include "Game.h"
#include "input.h"

// Constructor
Game::Game()
{ 
	input = new Input();
	pause = false;			//game is not paused
	graphics = NULL;
	initialized = false;

}


// Deconstructor
Game::~Game()
{
	deleteAll();			//free all reserved memory
	ShowCursor(true);		//show the cursor

}

//MessageHandler
LRESULT Game::messageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if(initialized)
	{
		switch(msg)								// look at the msg variable, then give different options
		{ 
		case WM_DESTROY:
			PostQuitMessage(0);					//Tells Windows OS to kill this program
			return 0;
		
		case WM_KEYDOWN: case WM_SYSKEYDOWN:	// key down
			input->keyDown(wParam);
			return 0;				
		
		case WM_KEYUP: case WM_SYSKEYUP:		// key up
			input->keyUp(wParam);
			return 0;
		
		case WM_CHAR:							// character entered
			input->keyIn(wParam);
			return 0;;
		
		case WM_MOUSEMOVE:						// mousemoved
			input->mouseIn(lParam);
			return 0;;
		
		case WM_INPUT:							// raw mouse data in
			input->mouseRawIn(lParam);
			return 0;

		case WM_LBUTTONDOWN:					// left mouse button down
			input->setMouseLButton(true);
			input->mouseIn(lParam);
			return 0;

		case WM_MBUTTONDOWN:					// middle mouse button down
			input->setMouseMButton(false);
			input->mouseIn(lParam);
			return 0;

		case WM_RBUTTONDOWN:					// right mouse button down
			input->setMouseRButton(false);
			input->mouseIn(lParam);
			return 0;

		case WM_DEVICECHANGE:					//check for controllers
			input->checkControllers();
			return 0;
		}
	}
	return DefWindowProc(hwnd, msg, wParam, lParam); // lets Window OS handle it
}

//intialize function
void Game::initialize(HWND hw)
{
	hwnd = hw; //save Winows handle


	//initialize graphics
	graphics = new Graphics();
	graphics->initialize(hwnd, GAME_WIDTH, GAME_HEIGHT, FULLSCREEN);


	//initilize input, do not capture mouwse
	input->initialize(hwnd, false);

	//set up high resolution timer
	if(QueryPerformanceFrequency(&timeFreq) == false)
	{
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing high res timer"));
	}

	QueryPerformanceCounter(&timeStart);		//get startng time
	initialized = true;
}


void Game::handleLostGraphicsDevice()
{
	// test for and handle lost device
	hr = graphics->getDeviceState();
	if(FAILED(hr))							// if the graphics device is not in a valid state
	{
		if(hr == D3DERR_DEVICELOST)			// if device is lost and not available for reset
		{
			Sleep(100);
			return;

		}
		else if (hr == D3DERR_DEVICENOTRESET)			// if devie was lost but is now available for reset
		{
			releaseAll();
			if(FAILED(hr)) return;			// if reset failed, return
			resetAll();
		}
		else
		{
			return;				// other device error
		}
	}
}

void Game::renderGame()
{
	if(SUCCEEDED(graphics->beginScene()) )
	{
		render(); // call render in derived class (SpaceWar)
		graphics->endScene();
	}
	handleLostGraphicsDevice();

	graphics->showBackbuffer(); // display the abckbuffer to the screen
}


// run function, called repeatedly in WinMain
void Game::run(HWND hwnd)
{
	if(graphics == NULL) // if graphics not initialized
	{
		return;
	}

	// calculate elapsed time of the last frame, save in frameTime
	QueryPerformanceCounter(&timeEnd);

	frameTime = (float) ( timeEnd.QuadPart - timeStart.QuadPart) /
				(float)timeFreq.QuadPart;


	// power saving code
	if(frameTime < MIN_FRAME_TIME)
	{
		sleepTime = (DWORD)((MIN_FRAME_TIME - frameTime)*1000);
		timeBeginPeriod(1);							// 1 mSec resolution of timer
		Sleep(sleepTime);							//release SPU for sleepTime
		timeEndPeriod(1);
		return;
	}

	if(frameTime > 0.0)
	{
		fps = (fps*0.99f) + (0.01f/frameTime);		// average fps

	}

	if(frameTime > MAX_FRAME_TIME)					// if frame rate is very slow
	{
		frameTime = MAX_FRAME_TIME;					// limit frameTime
	}

	timeStart = timeEnd;

	if(!pause)										// if not paused
	{
		update();									// update all game items
		ai();										// artificial intelligence
		collisions();								// handle collisions
		input->vibrateControllers(frameTime);		// handle controller vibrations
	}

	renderGame();									// draw all game items
	input->readControllers();						// read state of controllers
	
	//	clear input
	input->clear(inputNS::KEYS_PRESSED);	//
}

void Game::releaseAll()
{

}

void Game::resetAll()
{

}

void Game::deleteAll()
{
	releaseAll();
	SAFE_DELETE(graphics);
	SAFE_DELETE(input);
	initialized = false;
}