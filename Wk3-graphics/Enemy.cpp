// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 version 1.0

#include "Enemy.h"

//=============================================================================
// default constructor
//=============================================================================
Enemy::Enemy() : Entity()
{
    spriteData.x    = EnemyNS::X;              // location on screen
    spriteData.y    = EnemyNS::Y;
	collisionType = entityNS::CIRCLE;
	spriteData.rect.bottom = EnemyNS::HEIGHT * 0.5f;
	spriteData.rect.right = EnemyNS::WIDTH * 0.5f;
	spriteData.rect.left = EnemyNS::WIDTH * -0.5f;
	spriteData.rect.top = EnemyNS::HEIGHT * -0.5f;
	
//  radius          = EnemyNS::COLLISION_RADIUS;
    mass            = EnemyNS::MASS;
    startFrame      = EnemyNS::START_FRAME;    // first frame of ship animation
    endFrame        = EnemyNS::END_FRAME;      // last frame of ship animation
    setCurrentFrame(startFrame);
}