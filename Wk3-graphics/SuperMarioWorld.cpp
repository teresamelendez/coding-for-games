#include "SuperMarioWorld.h"
#include "graphics.h"
#include "Player.h"
#include "entity.h"
#include "image.h"
#include "Environment.h"
#include "Text.h"

SuperMarioWorld::SuperMarioWorld()
{

}

SuperMarioWorld::~SuperMarioWorld()
{


	releaseAll();
}

void SuperMarioWorld::initialize(HWND hwnd)
{
	Game::initialize( hwnd );		// call game's initialize
	if ( !backgroundTexture.initialize(graphics, SUPERWORLD_IMAGE) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing Hills image") );
	}


	if ( !gameTextures.initialize(graphics, TEXTURES_IMAGE) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures") );

	}

	if (!worldTexture.initialize(graphics, SUPERWORLD_IMAGE))
	{
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing space texture"));
	}

	/*
	if( graphics->isSpriteNull() )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "graphics sprite is null!") );
	}

	if( this->getGraphics()->isSpriteNull()) 
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "this->getGraphics() sprite is null!") );
	}
	*/
	
	if ( !mario.initialize(this, PlayerNS::WIDTH,	PlayerNS::HEIGHT, PlayerNS::TEXTURE_COLS, &gameTextures) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing Mario!") );

	}

	if ( !bullet.initialize(this, EnemyNS::WIDTH, EnemyNS::HEIGHT, EnemyNS::TEXTURE_COLS, &gameTextures) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing bullet enemy") );

	}	

	if( !floor.initialize(this, EnvironmentNS::WIDTH, EnvironmentNS::HEIGHT, EnvironmentNS::TEXTURE_COLS, &gameTextures) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing environment") );
	}

	if( !startAndEnd.initialize(graphics, FONT_IMAGE) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing text") );
	}

	if(!world.initialize(graphics,0,0,0,&worldTexture))
	{
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing world"));
		world.setScale((float) WORLD_SCALE);
	}


	// place planet in the center of the screen
	//bullet.setX(GAME_WIDTH * 0.5f - bullet.getWidth() * 0.5f);
	//bullet.setY(GAME_HEIGHT * 0.5f - bullet.getHeight() * 0.5f);
	bullet.setX(550);
	bullet.setY(310);

	if(mario.getX() >= -20 && mario.getX() < 25)
	{
		bullet.setX(bullet.getX() + 10);
	}

	// starting screen position for the ship1
	mario.setX(0);
	mario.setY(387);



//	queBlocks.setX(386);
	//queBlocks.setY(307);

	// turns on the animation (lets Image know this image is animated)
	mario.setFrames(PlayerNS::SHIP1_START_FRAME, PlayerNS::SHIP1_END_FRAME);		// set up the animation frames
	mario.setCurrentFrame(PlayerNS::SHIP1_START_FRAME);					// starting frame
	mario.setFrameDelay(PlayerNS::SHIP_ANIMATION_DELAY);				// how long to show each frame
	mario.setVelocity(VECTOR2(0, -PlayerNS::SPEED));
	
	floor.setX(-21);
	floor.setY(320);

	/*
	if ( !ship2.initialize(this, ShipNS::WIDTH, ShipNS::HEIGHT, ShipNS::TEXTURE_COLS, &gameTextures) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship2") );

	}
	ship2.setX(GAME_WIDTH / 4);
	ship2.setY(GAME_WIDTH / 4);

	// turns on the animation (lets Image know this image is animated)
	ship2.setFrames(ShipNS::SHIP2_START_FRAME, ShipNS::SHIP2_END_FRAME);		// set up the animation frames
	ship2.setCurrentFrame(ShipNS::SHIP2_START_FRAME);					// starting frame
	ship2.setFrameDelay(ShipNS::SHIP_ANIMATION_DELAY);				// how long to show each frame
	ship2.setVelocity(VECTOR2(0, -ShipNS::SPEED));
	*/

	return;
}

void SuperMarioWorld::update()
{
	// makes the ship1 attracted to the planet
//	ship1.gravityForce(&planet, frameTime);
	//ship2.gravityForce(&planet, frameTime);
	
	
	floor.update(frameTime);

	mario.update(frameTime);
	//ship2.update(frameTime);

	bullet.update(frameTime);

	// Move space in X direction opposite ship
	if(mario.getX() > GAME_WIDTH) //	if offscreen right
	{
	world.setX(world.getX() - frameTime * mario.getVelocity().x);
	}
	// Move space in Y direction opposite ship
	world.setY(world.getY() - frameTime * mario.getVelocity().y);
	// Wrap space image around at edge
	// If left edge of world > screen left edge
	if (world.getX() > 0)
		// Move space image left by WORLD_WIDTH
		world.setX(world.getX() - WORLD_WIDTH);
	// If space image offscreen left
	if (world.getX() < - WORLD_WIDTH)
		// Move space image right by WORLD_WIDTH
		world.setX(world.getX() + WORLD_WIDTH);
	// If top edge of space > screen top edge
	if (world.getY() > 0)
		// Move space image up by SPACE_HEIGHT
		world.setY(world.getY() - WORLD_HEIGHT);
	// If space image offscreen top
	if (world.getY() < -WORLD_HEIGHT)
		// Move space image down by SPACE_IMAGE
		world.setY(world.getY() + WORLD_HEIGHT);





	float SPEED = 100.0f;
	//	rotate
	// ship1.setDegrees(ship1.getDegrees() +  frameTime * 180.0f);

	// scale
	//ship1.setScale(ship1.getScale() - frameTime * 0.2f);


	//ship1.setX(ship1.getX() + frameTime * 100.0f);
/*
	if(mario.getX() > GAME_WIDTH) //	if offscreen right
	{
		mario.setX( (float)mario.getWidth() );
		mario.setScale( 1.5f); // reset the scale to 1.5
	}
*/
	if(input->isKeyDown(SHIP_RIGHT_KEY) )
	{
		if(mario.getX() > GAME_WIDTH - mario.getWidth())
		{
		mario.setX(mario.getX() + frameTime * SPEED);
		}
		
		mario.setX(mario.getX() + frameTime * 100.0f);
		mario.flipHorizontal(true);
	}
	else if(!input->isKeyDown(SHIP_RIGHT_KEY))
	{
		mario.setDegrees(mario.getDegrees());

	}
	if(input->isKeyDown(SHIP_LEFT_KEY) )
	{
		if(mario.getX() > 0)
		{
		mario.setX(mario.getX() - frameTime * SPEED);
		}
		mario.setX(mario.getX() - frameTime * 100.0f);
		mario.flipHorizontal(true);
	}
	else if(!input->isKeyDown(SHIP_LEFT_KEY) && input->isKeyDown(SHIP_RIGHT_KEY))
	{
		mario.flipHorizontal(false);
	}
	if(input->isKeyDown(SHIP_UP_KEY) )
	{
		if(mario.getY() > GAME_HEIGHT - mario.getHeight())
		{
		mario.setY(mario.getY() + frameTime * SPEED);
		}
		mario.setY(mario.getY() - frameTime * SPEED);
		//ship1.setDegrees(ship1.getDegrees() -  frameTime * 160.0f);
		mario.setY(mario.getY() - 1);
	}
	if(input->isKeyDown(SHIP_DOWN_KEY) )
	{
		if(mario.getY() > 0)
		{
		mario.setY(mario.getY() - frameTime * SPEED);
		}

		mario.setY(mario.getY() + frameTime * SPEED);
		//ship1.setDegrees(ship1.getDegrees() +  frameTime * 160.0f);
		mario.setY(mario.getY() + 1);
	}
	/*
	if(input->isKeyDown(SHIP_RIGHT_KEY) && input->isKeyDown(SHIP_UP_KEY))
	{
		if(ship1.getDegrees() < -90.0f)
		{
		ship1.setDegrees(ship1.getDegrees() - 1.0f);
		}
		/*
		if(ship1.getDegrees() > 90.0f)
		{
			ship1.setDegrees(ship1.getDegrees() + 1.0f);
		}
		*/
		/*
		if(ship1.getDegrees() >  ship1.getDegrees() + 90.0f)
		{
			ship1.setDegrees(ship1.getDegrees() - 89.0f);
		}
		if(ship1.getDegrees() <  ship1.getDegrees() - 90.0f  )
		{
			ship1.setDegrees(ship1.getDegrees() - 89.0f);
		}
		*/
}





void SuperMarioWorld::ai()
{
}


void SuperMarioWorld::collisions()
{
	VECTOR2 collisionVector;
	if(mario.collidesWith(bullet, collisionVector))
	{
		// bounce off the planet
		mario.bounce(collisionVector, bullet);
		mario.damage(PLANET);
	}
	if(mario.collidesWith(floor, collisionVector))
	{
		// bounce off the planet
		mario.bounce(collisionVector, floor);
		///mario.damage(PLANET);
	}
/*
	if(mario.collidesWith(bullet, collisionVector))
	{
		// bounce off the planet
		mario.bounce(collisionVector, bullet);
		mario.damage(PLANET);
	}
	*/
	/*
	if(mario.collidesWith(mario, collisionVector))
	{
		// bounce off the planet
		mario.bounce(collisionVector, ship2);
		ship1.damage(SHIP);
		ship2.bounce(collisionVector * -1, ship1);
		ship2.damage(SHIP);
	}*/
	
	/*
	if(mario.collidesWith(queBlocks, collisionVector))
	{
		// bounce off the planet
		mario.bounce(collisionVector, queBlocks);
		// mario.damage(PLANET);
	}
	*/

}

void SuperMarioWorld::render()
{
	graphics->spriteBegin();
	// call draw functions afte! 
	
	float x = world.getX();
	float y = world.getY();
	// Begin drawing sprites
	// Wrap space image around at edges
	world.draw();
	// Draw at current location
	// If space image right edge visible
	if (world.getX() < -WORLD_WIDTH + (int)GAME_WIDTH)
	{
		world.setX(world.getX() + WORLD_WIDTH);
	// Wrap around to left edge
		world.draw();
		// Draw again
	}
	// If space image bottom edge visible
	if (world.getY() < -WORLD_HEIGHT + (int)GAME_HEIGHT)
	{ 
		world.setY(world.getY() + WORLD_HEIGHT);
	// Wrap around to top edge
	world.draw();
	}
	// Draw again
	world.setX(x);
	// Restore X position
	// If space image right edge visible
	// wrap around to left edge
	if(world.getX() < -WORLD_WIDTH + (int)GAME_WIDTH)
	{
		world.draw();
		// Draw again
	}
	world.setY(y);
	// Restore Y position

	world.draw();
	// floor.draw();
	bullet.draw();
	mario.draw();

	if(mario.getX() >= -20 && mario.getX() < 25)
	{
	startAndEnd.print("L", 50, 162);
	startAndEnd.print("E", 50 + textNS::FONT_WIDTH, 162);
	startAndEnd.print("T", 50 + textNS::FONT_WIDTH * 2, 162);
	startAndEnd.print("'", 50 + textNS::FONT_WIDTH * 3, 162);
	startAndEnd.print("S ", 50 + textNS::FONT_WIDTH * 4 , 162);
	startAndEnd.print("G", 50 + textNS::FONT_WIDTH * 6, 162);
	startAndEnd.print("O", 50 + textNS::FONT_WIDTH * 7, 162);
	startAndEnd.print("!", 50 + textNS::FONT_WIDTH * 8 - 2, 162);
	}

	if(mario.getX() >= 1000 && mario.getX() < 1100)
	{
		startAndEnd.print("Y",1000, 162);
		startAndEnd.print("A",1000 + textNS::FONT_WIDTH, 162);
		startAndEnd.print("Y", 1000 + textNS::FONT_WIDTH * 2, 162);
		startAndEnd.print("!", 1000 + textNS::FONT_WIDTH * 3 - 2, 162);

	}




	//call drawfunctions before!
	graphics->spriteEnd();
}

void SuperMarioWorld::releaseAll()
{
	gameTextures.onLostDevice();
	backgroundTexture.onLostDevice();
	

	Game::releaseAll();

}

void SuperMarioWorld::resetAll()
{
	gameTextures.onResetDevice();
	backgroundTexture.onResetDevice();

	Game::resetAll();

}