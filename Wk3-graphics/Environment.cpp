// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 version 1.0

#include "Environment.h"

//=============================================================================
// default constructor
//=============================================================================
Environment::Environment() : Entity()
{
    spriteData.x    = EnvironmentNS::X;              // location on screen
    spriteData.y    = EnvironmentNS::Y;
	collisionType = entityNS::BOX;
    mass            = EnvironmentNS::MASS;
    startFrame      = EnvironmentNS::START_FRAME;    // first frame of environment animation
    endFrame        = EnvironmentNS::END_FRAME;      // last frame of environment animation
    setCurrentFrame(startFrame);
}