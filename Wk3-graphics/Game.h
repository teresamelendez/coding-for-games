//Game.h

#ifndef _GAME_H_ //prevents multiple definitions of the class
#define _GAME_H_ //prevents multiple definitions of the class

#define WIN32_LEAD_AND_MEAN 

#include <Windows.h>
#include <MMSystem.h>
#include "graphics.h"
#include "constants.h"
#include "gameError.h"
#include "input.h"


class Game
{

protected:
//allows to access the variables provided


	Input* input;
	Graphics* graphics;			//pointer to Graphics
	HWND hwnd;					//windows handle
	HRESULT hr;					//return type from Windows/DX
	LARGE_INTEGER timeStart;	//performance counter start value
	LARGE_INTEGER timeEnd;		//perfor mance counter end value
	LARGE_INTEGER timeFreq;		// performance counter frequency
	float frameTime;			// time required for last frame
	float fps;					// frames per second
	DWORD sleepTime;			// number of millseconds to sleep between frames
	bool pause;					// use to pause game
	bool initialized;			//true if initialized


//private:						//cannot be accessed beyond the class



public:							// can be accessed beyond the class
	Game();						// Constructor
	virtual ~Game();			// Deconstructor

	// **MEMBER FUNCTIONS

	// Window Message Handler
	LRESULT messageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	
	// Initialization function
	virtual void initialize(HWND hwnd);

	// call run repeatedly by the main message loop in WinMain
	 virtual void run(HWND); //named the type of the input but have not defined the input name

	 // call when the grapgics devixw ia lost
	 virtual void releaseAll();

	// Recreate all surfaces and reset all entities
	 virtual void resetAll();
	
	 // delete all reserved memory
	 virtual void deleteAll();

	 // render game items
	 virtual void renderGame();

	 // hand lost graphics device
	 virtual void handleLostGraphicsDevice();

	 // return pointer to graphics device
	 Graphics* getGraphics() {return graphics;}

	 Input* getInput() {return input;}

	 // exit the game
	 void exitGame() { PostMessage(hwnd, WM_DESTROY, 0, 0);} //


	 // pure virtual function declaration
	 // update game items
	 virtual void update() = 0;


	 //perfrom Artificial Intelligence calculations
	 virtual void ai() = 0;


	 //check for collisions
	 virtual void collisions() = 0;


	 //render graphics
	 virtual void render() = 0;









};



#endif //prevents multiple definitions of the class