#include "SpaceWar.h"
#include "graphics.h"


SpaceWar::SpaceWar()
{

}

SpaceWar::~SpaceWar()
{


	releaseAll();
}

void SpaceWar::initialize(HWND hwnd)
{
	Game::initialize( hwnd );		// call game's initialize
	if ( !nebulaTexture.initialize(graphics, NEBULA_IMAGE) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula texture") );

	}


	if ( !planetTexture.initialize(graphics, PLANET_IMAGE) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing planet texture") );

	}


	if ( !nebula.initialize(graphics, 0, 0, 0, &nebulaTexture) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula texture") );

	}

	if ( !planet.initialize(graphics, 0, 0, 0, &planetTexture) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing planet texture") );

	}


	// place planet in the center of the screen
	planet.setX(GAME_WIDTH * 0.5f - planet.getWidth() * 0.5f);
	planet.setY(GAME_HEIGHT * 0.5f - planet.getHeight() * 0.5f);

}

void SpaceWar::update()
{


}

void SpaceWar::ai()
{
}


void SpaceWar::collisions()
{


}

void SpaceWar::render()
{
	graphics->spriteBegin();
	// call draw functions afte! 

	nebula.draw();
	planet.draw();
	
	//call drawfunctions before!
	graphics->spriteEnd();
}

void SpaceWar::releaseAll()
{
	planetTexture.onLostDevice();
	nebulaTexture.onLostDevice();



	Game::releaseAll();

}

void SpaceWar::resetAll()
{
	planetTexture.onResetDevice();
	nebulaTexture.onResetDevice();

	Game::resetAll();

}