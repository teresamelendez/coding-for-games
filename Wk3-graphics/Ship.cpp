#define WIN 32_LEAN_AND_MEAN

#include "Ship.h"
#include "entity.h"

Ship::Ship() : Entity()
{
	spriteData.width = ShipNS::WIDTH;
	spriteData.height = ShipNS::HEIGHT;
	spriteData.x = ShipNS:: X;
	spriteData.y = ShipNS::Y;
	spriteData.rect.bottom = ShipNS::HEIGHT;
	spriteData.rect.right = ShipNS::WIDTH;
	frameDelay = ShipNS::SHIP_ANIMATION_DELAY;
	startFrame = ShipNS::SHIP1_START_FRAME;
	endFrame = ShipNS::SHIP1_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = ShipNS::WIDTH/2.0f;


	velocity.x = 0;
	velocity.y = 0;
}

void Ship::update(float frameTime)
{
	Entity::update(frameTime);

	// rotation and position
	//spriteData.angle += frameTime * ShipNS::ROTATION_RATE;
	//spriteData.x += frameTime * velocity.x;
	//spriteData.y += frameTime * velocity.y;

	// bounce off the wall off the screen
	if(spriteData.x > GAME_WIDTH - ShipNS::WIDTH * getScale() )
	{
		// push it back onto the screem if it's gone past the edge
		spriteData.x = GAME_WIDTH - ShipNS::WIDTH * getScale();
		velocity.x = -velocity.x;
	}
	else if(spriteData.x < 0)
	{
		spriteData.x = 0;
		velocity.x = -velocity.x;
	}
	if(spriteData.y > GAME_HEIGHT - ShipNS::HEIGHT * getScale() )
	{
		// push it back onto the screem if it's gone past the edge
		spriteData.y = GAME_HEIGHT - ShipNS::HEIGHT * getScale();
		velocity.y = -velocity.y;
	}
	else if(spriteData.y < 0)
	{
		spriteData.y = 0;
		velocity.y = -velocity.y;
	}

	if(shieldOn)
	{
		shield.update(frameTime);
		if(shield.getAnimationComplete())
		{
			shieldOn = false;
			shield.setAnimationComplete(false);	// reset animation

		}

	}


}

bool Ship::initialize(Game *gamePtr, int width, int height, int ncols, TextureManager *texturePtr)
{

	shield.initialize(gamePtr->getGraphics(), width, height, ncols, texturePtr);
	shield.setFrames(ShipNS::SHIELD_START_FRAME, ShipNS::SHIELD_END_FRAME);
	shield.setCurrentFrame(ShipNS::SHIELD_START_FRAME);
	shield.setFrameDelay(ShipNS::SHIELD_ANIMATION_DELAY);
	shield.setLoop(false); // don't want to loop the animation
	return(Entity::initialize(gamePtr, width, height, ncols, texturePtr));
}

void Ship::draw()
{
	Image::draw();	// draw the ship first
	
	if(shieldOn)	// if the shield is on, draw it on top of the ship
	{
		shield.draw(spriteData, graphicsNS::ALPHA50 & colorFilter);
		// ship's spriteData -- shield will be the same size and rotation as the ship
	}

}

void Ship::damage(WEAPON weapon)
{
	shieldOn = true;
}