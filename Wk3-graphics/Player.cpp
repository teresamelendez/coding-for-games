#define WIN 32_LEAN_AND_MEAN

#include "Player.h"
#include "entity.h"

Player::Player() : Entity()
{
	spriteData.width = PlayerNS::WIDTH;
	spriteData.height = PlayerNS::HEIGHT;
	spriteData.x = PlayerNS::X;
	spriteData.y = PlayerNS::Y;
	spriteData.rect.bottom = PlayerNS::HEIGHT * 0.5f;
	spriteData.rect.right = PlayerNS::WIDTH * 0.5f;
	spriteData.rect.left = PlayerNS::WIDTH * -0.5f;
	spriteData.rect.top = PlayerNS::HEIGHT * -0.5f;
	frameDelay = PlayerNS::SHIP_ANIMATION_DELAY;
	startFrame = PlayerNS::SHIP1_START_FRAME;
	endFrame = PlayerNS::SHIP1_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = PlayerNS::WIDTH/2.0f;


	velocity.x = 0;
	velocity.y = 0;
}

void Player::update(float frameTime)
{
	Entity::update(frameTime);

	// rotation and position
	// spriteData.angle += frameTime * PlayerNS::ROTATION_RATE;
	// spriteData.x = -1000;
	 // spriteData.y = -100;

	// bounce off the wall off the screen
	if(spriteData.x > GAME_WIDTH - PlayerNS::WIDTH * getScale() )
	{
		// push it back onto the screem if it's gone past the edge
		spriteData.x = GAME_WIDTH - PlayerNS::WIDTH * getScale();
		velocity.x = -velocity.x;
	}
	else if(spriteData.x < 0)
	{
		spriteData.x = 0;
		velocity.x = -velocity.x;
	}
	if(spriteData.y > GAME_HEIGHT - PlayerNS::HEIGHT * getScale() )
	{
		// push it back onto the screem if it's gone past the edge
		spriteData.y = GAME_HEIGHT - PlayerNS::HEIGHT * getScale();
		velocity.y = -velocity.y;
	}
	else if(spriteData.y < 0)
	{
		spriteData.y = 0;
		velocity.y = -velocity.y;
	}

	if(spriteData.y < -100)
	{
		velocity.y = 0;
	}
	if(shieldOn)
	{
		shield.update(frameTime);
		if(shield.getAnimationComplete())
		{
			shieldOn = false;
			shield.setAnimationComplete(false);	// reset animation
		}

	}


}

bool Player::initialize(Game *gamePtr, int width, int height, int ncols, TextureManager *texturePtr)
{

	shield.initialize(gamePtr->getGraphics(), width, height, ncols, texturePtr);
	shield.setFrames(PlayerNS::SHIELD_START_FRAME, PlayerNS::SHIELD_END_FRAME);
	shield.setCurrentFrame(PlayerNS::SHIELD_START_FRAME);
	shield.setFrameDelay(PlayerNS::SHIELD_ANIMATION_DELAY);
	shield.setLoop(false); // don't want to loop the animation
	return(Entity::initialize(gamePtr, width, height, ncols, texturePtr));
}

void Player::draw()
{
	Image::draw();	// draw the ship first
	
	if(shieldOn)	// if the shield is on, draw it on top of the ship
	{
		shield.draw(spriteData, graphicsNS::ALPHA50 & colorFilter);
		// ship's spriteData -- shield will be the same size and rotation as the ship
	}

}

void Player::damage(WEAPON weapon)
{
	shieldOn = true;
}