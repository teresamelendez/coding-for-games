#ifndef _SHIP_H
#define _SHIP_H
#define WIN 32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace ShipNS
{
// related 
const int TEXTURE_COLS = 8;					// ship texture has two 2 columns
const int WIDTH  = 32;						// width of the ship image
const int HEIGHT = 32;						// height of the ship image

const int X = GAME_WIDTH/2 - WIDTH/2;
const int Y = GAME_HEIGHT/2 - HEIGHT/2;

const float SPEED = 100.0f;
const float MASS = 300.0f;
const float ROTATION_RATE = (float)PI/4;

// ship related constants
const int SHIP_COLS = 8;							// ship texture has two 2 columns
const int SHIP_WIDTH  = 32;							// width of the ship image
const int SHIP_HEIGHT = 32;							// height of the ship image

const int SHIP1_START_FRAME = 0;					// starting rame of animation for ship 1
const int SHIP1_END_FRAME = 3;						// ending frame of animation for ship 1
const int SHIP2_START_FRAME = 0;					// starting frame of animation for ship 2
const int SHIP2_END_FRAME = 3;						// ending frame of animation for ship 2


const float SHIP_ANIMATION_DELAY = 0.2f;	// time between frame of the ship animation

const int SHIELD_START_FRAME = 24;
const int SHIELD_END_FRAME = 27;
const float  SHIELD_ANIMATION_DELAY = 0.1f;


}

class Ship : public Entity
{
private:
	bool shieldOn;					// store if the shield is on
	Image shield;					//

public:
	Ship();
	void update(float frameTime);

	virtual void draw();
	virtual bool initialize( Game *gamePtr, int width, int height, int ncols, TextureManager *texturePtr);
	
	void damage(WEAPON);

};

#endif

