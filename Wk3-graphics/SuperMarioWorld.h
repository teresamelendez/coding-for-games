#ifndef _SPACEWAR_
#define _SPACEWAR_
#define WIN32_LEAD_AND_MEAN

#include "Game.h"
#include "textureManager.h"
#include "image.h"
#include "graphics.h"
#include "Player.h"
#include "Enemy.h"
#include "Text.h"
#include "Environment.h"

class SuperMarioWorld : public Game
{
public:
	SuperMarioWorld();						//constructor
	virtual ~SuperMarioWorld();			//deconstructor

	void(initialize( HWND hwnd));
	void update();					// override pure virtual from Game
	void ai();						// override pure virtual from Game
	void collisions();				// override pure virtual from Game
	void render();					// override pure virtual from Game

	void releaseAll();
	void resetAll();


private:
	// variables
	TextureManager backgroundTexture;	// nebula texture
//	TextureManager planetTexture;	// planet texture
//	TextureManager shipTexture;		// ship texture
	TextureManager gameTextures;
	TextureManager worldTexture;

	Image world;			// Background Image
	Enemy bullet;				// Bullet Enemy image
	Player mario;				// Mario image
	Environment floor;			// Environment image
	Text startAndEnd;
};


#endif