// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 3 constants.h v1.0

#ifndef _CONSTANTS_H            // prevent multiple definitions if this 
#define _CONSTANTS_H            // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }
// safely call on onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr) {if(ptr) {ptr->onLostDevice();} }
// safely call on onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr) {if(ptr) {ptr->onResetDevice();} }
// transparency color (the color that's considered to be transparent)
#define TRANSCOLOR SETCOLOR_ARGB(0,255,0,255)	// transparency color (magenta)

//-----------------------------------------------
//                  Constants	
//-----------------------------------------------

// window
const char CLASS_NAME[] = "WinMain";
const char GAME_TITLE[] = "DirectX Window";
const bool FULLSCREEN = false;					    // windowed or fullscreen
const UINT  GAME_WIDTH = 2473  / 2;					    // width of game in pixels
const UINT  GAME_HEIGHT = 432;					    // height of game in pixels

//game
const double PI= 3.14159265;
const float FRAME_RATE = 200.0f;					//the target frame rate
const float MIN_FRAME_RATE = 10.0f;					// minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;		//minimum desired time for one frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE;	//maximum time
const int WORLD_SCALE = 2;
const int WORLD_WIDTH = 640 * WORLD_SCALE;
const int WORLD_HEIGHT = 480 * WORLD_SCALE;


// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY = VK_ESCAPE;					// escape key
const UCHAR SHIP_LEFT_KEY = VK_LEFT;
const UCHAR SHIP_RIGHT_KEY = VK_RIGHT;
const UCHAR SHIP_UP_KEY = VK_UP;
const UCHAR SHIP_DOWN_KEY = VK_DOWN;
const UCHAR Q_KEY = 0x51;
const UCHAR E_KEY = 0x45;



// graphics image
const char SUPERWORLD_IMAGE[] = "pictures\\BG.png";
const char TEXTURES_IMAGE[] = "pictures\\MarioTextures.png";
const char FONT_IMAGE[] = "pictures\\CKfont.png";

// weapon types
enum WEAPON{ OBJECTS, SHIP, PLANET};


#endif
