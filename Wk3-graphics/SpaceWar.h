#ifndef _SPACEWAR_
#define _SPACEWAR_
#define WIN32_LEAD_AND_MEAN

#include "Game.h"
#include "textureManager.h"
#include "image.h"
#include "graphics.h"



class SpaceWar : public Game
{
public:
	SpaceWar();				//constructor
	virtual ~SpaceWar();	//deconstructor

	void(initialize( HWND hwnd));
	void update();					// override pure virtual from Game
	void ai();						// override pure virtual from Game
	void collisions();				// override pure virtual from Game
	void render();					// override pure virtual from Game

	void releaseAll();
	void resetAll();


private:
	// variables
	TextureManager nebulaTexture;	// nebula texture
	TextureManager planetTexture;	// planet texture

	Image nebula;					// nebula image
	Image planet;					// planet image



};


#endif